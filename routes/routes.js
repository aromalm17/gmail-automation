const express = require("express");
const path = require("path");
const { authenticate } = require("@google-cloud/local-auth");
const controllers = require("../controllers/controllers");

const router = express.Router();

const SCOPES = [
  "https://www.googleapis.com/auth/gmail.readonly",
  "https://www.googleapis.com/auth/gmail.send",
  "https://www.googleapis.com/auth/gmail.labels",
  "https://mail.google.com/",
];


router.get("/", async (req, res) => {
  try {
    const credentialsDirectory = "../credentials";
    const keyfilePath = path.join(
      __dirname,
      credentialsDirectory,
      "credentials.json"
    );

    const auth = await authenticate({
      keyfilePath: keyfilePath,
      scopes: SCOPES,
    });

    const labelId = await controllers.createLabel(auth);
    const LABEL_NAME = "Auto Mail";
    console.log(`Created or found label ${LABEL_NAME}`);
    setInterval(async () => {
      const messages = await controllers.getUnrepliedMessages(auth, labelId);
      console.log(
        `Processing ${messages.length
        } messages at ${new Date().toLocaleTimeString()}`
      );
      for (const message of messages) {
        await controllers.sendReply(auth, message);
        console.log(`Sent reply to message with id ${message.id}`);

        await controllers.addLabel(auth, message, labelId);
        console.log(`Added label to message with id ${message.id}`);
      }
    }, Math.floor(Math.random() * (120 - 45 + 1) + 45) * 1000);

    res.send("Successfully Activated");
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
