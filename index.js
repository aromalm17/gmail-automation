const express = require('express');
const routes = require('./routes/routes');
const dotenv = require('dotenv').config();

const app = express();

app.use('/', routes);

app.listen(process.env.PORT, () => {
  console.log(`App listening at http://localhost:${process.env.PORT}`);
});
